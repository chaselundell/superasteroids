package edu.byu.cs.superasteroids.model;

import edu.byu.cs.superasteroids.drawing.DrawingHelper;

/**
 * Created by chaselundell on 2/11/2016.
 * visibleObjects class will be inherited by all objects that are visible in gamePlay
 */
public class visibleObjects {

    public float xPos;
    public float yPos;

    public visibleObjects(){
        xPos = 0;
        yPos = 0;
    }

    public float getxPos() {
        return xPos;
    }

    public void setxPos(float xPos) {
        this.xPos = xPos;
    }

    public float getyPos() {
        return yPos;
    }

    public void setyPos(float yPos) {
        this.yPos = yPos;
    }

    /**
     * This function will draw the specified image on the screen.
     * @param img
     * @param x
     * @param y
     */
    public void drawImage(int img, float x, float y, float rotationDegrees, float scaleX, float scaleY, int opacity){
        DrawingHelper.drawImage(img, x, y, rotationDegrees, scaleX, scaleY, opacity);
    }

    /**
     * This function will update the positions of the visible object
     */
    public void updateObjectCoordinates(){
        //this is from the middle of the ship if the object is a ship
    }
}
