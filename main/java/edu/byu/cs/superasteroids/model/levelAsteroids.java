package edu.byu.cs.superasteroids.model;

/**
 * Created by chaselundell on 2/11/2016.
 */
public class levelAsteroids extends movingObjects{
    private int levelNum;
    private int numOfAsteroids;
    private int asteroidId;
    public levelAsteroids(){
        levelNum = 0;
        numOfAsteroids = 0;
        asteroidId = 0;
    }

    public int getLevelNum() {
        return levelNum;
    }

    public void setLevelNum(int levelNum) {
        this.levelNum = levelNum;
    }

    public int getNumOfAsteroids() {
        return numOfAsteroids;
    }

    public void setNumOfAsteroids(int numOfAsteroids) {
        this.numOfAsteroids = numOfAsteroids;
    }

    public int getAsteroidId() {
        return asteroidId;
    }

    public void setAsteroidId(int asteroidId) {
        this.asteroidId = asteroidId;
    }
}
