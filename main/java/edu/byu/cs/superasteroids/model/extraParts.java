package edu.byu.cs.superasteroids.model;

/**
 * Created by chaselundell on 2/11/2016.
 */
public class extraParts {
    private int attachPointX;
    private int attachPointY;
    private String image;
    private int imageWidth;
    private int imageHeight;
    private Integer convertedImage;

    public extraParts(){
        attachPointX = 0;
        attachPointY = 0;
        image = "";
        imageWidth = 0;
        imageHeight = 0;
        convertedImage = 0;
    }

    public Integer getConvertedImage() {
        return convertedImage;
    }

    public void setConvertedImage(Integer convertedImage) {
        this.convertedImage = convertedImage;
    }

    public int getAttachPointX() {
        return attachPointX;
    }

    public void setAttachPointX(int attachPointX) {
        this.attachPointX = attachPointX;
    }

    public int getAttachPointY() {
        return attachPointY;
    }

    public void setAttachPointY(int attachPointY) {
        this.attachPointY = attachPointY;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getImageWidth() {
        return imageWidth;
    }

    public void setImageWidth(int imageWidth) {
        this.imageWidth = imageWidth;
    }

    public int getImageHeight() {
        return imageHeight;
    }

    public void setImageHeight(int imageHeight) {
        this.imageHeight = imageHeight;
    }
}
