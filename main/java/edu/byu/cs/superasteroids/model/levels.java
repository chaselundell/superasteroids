package edu.byu.cs.superasteroids.model;

/**
 * Created by chaselundell on 2/11/2016.
 */
public class levels {
    private int levelId;
    private int width;
    private int height;
    private int number;
    private String title;
    private String hint;
    private String music;
    public levels(){
        levelId = 0;
        width = 0;
        height = 0;
        number = 0;
        title = "";
        hint = "";
        music = "";
    }

    public int getLevelId() {
        return levelId;
    }

    public void setLevelId(int levelId) {
        this.levelId = levelId;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }
}
