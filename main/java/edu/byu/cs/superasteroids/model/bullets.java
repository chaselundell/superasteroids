package edu.byu.cs.superasteroids.model;

import android.graphics.Point;

/**
 * Created by chaselundell on 2/11/2016.
 */
public class bullets extends visibleObjects{
    private String bulletImage;
    private Point bulletSpeed;
    private float angle;
    private int bulletWidth;
    private int bulletHeight;

    public bullets(){
        bulletImage = "";
        angle = 0;
        bulletWidth = 0;
        bulletHeight = 0;
    }

    public int getBulletWidth() {
        return bulletWidth;
    }

    public void setBulletWidth(int bulletWidth) {
        this.bulletWidth = bulletWidth;
    }

    public int getBulletHeight() {
        return bulletHeight;
    }

    public void setBulletHeight(int bulletHeight) {
        this.bulletHeight = bulletHeight;
    }

    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    public String getBulletImage() {
        return bulletImage;
    }

    public void setBulletImage(String bulletImage) {
        this.bulletImage = bulletImage;
    }

    public Point getBulletSpeed() {
        return bulletSpeed;
    }

    public void setBulletSpeed(Point bulletSpeed) {
        this.bulletSpeed = bulletSpeed;
    }
}
