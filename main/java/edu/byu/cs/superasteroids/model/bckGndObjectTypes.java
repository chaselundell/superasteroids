package edu.byu.cs.superasteroids.model;

/**
 * Created by chaselundell on 2/11/2016.
 */
public class bckGndObjectTypes {
    private int bckGndObjectId;
    private String imgPath;

    public bckGndObjectTypes(){
        bckGndObjectId = 0;
        imgPath = "";
    }

    public int getBckGndObjectId() {
        return bckGndObjectId;
    }

    public void setBckGndObjectId(int bckGndObjectId) {
        this.bckGndObjectId = bckGndObjectId;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }
}
