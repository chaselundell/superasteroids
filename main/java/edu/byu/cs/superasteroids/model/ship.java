package edu.byu.cs.superasteroids.model;

import android.graphics.PointF;

import edu.byu.cs.superasteroids.core.GraphicsUtils;
import edu.byu.cs.superasteroids.drawing.DrawingHelper;

/**
 * Created by chaselundell on 2/11/2016.
 */
public class ship extends movingObjects{
    private float shipWidth;
    private float shipHeight;
    private cannons cannon;
    private extraParts extraPart;
    private engines engine;
    private float angle;
    private mainBodies mainBody;
    private powerCores powerCore;
    private PointF emitPos;
    public ship(){
        shipHeight = 0;
        shipWidth = 0;
        angle = 0;
        cannon = new cannons();
        extraPart = new extraParts();
        engine = new engines();
        mainBody = new mainBodies();
        powerCore = new powerCores();

    }

    public void drawBullets(float bulletX, float bulletY, float angle, float xScale, float yScale){
        DrawingHelper.drawImage(cannon.getAttackImageConverted(), bulletX, bulletY, angle, xScale, yScale, 255);
    }

//    * @param imageId The ID of an image that has already been loaded through the content manager
//    * @param x The x coordinate to draw the center of the image at
//    * @param y The y coordinate to draw the center of the image at
//    * @param rotationDegrees The rotation on the image in degrees
//    * @param scaleX The image X scale
//    * @param scaleY The image Y scale
//    * @param alpha Image alpha (0 - 255, 0 = completely transparent, 255 = completely opaque)
    public void drawShip(float shipX, float shipY, float xScale, float yScale){
        float partAttachX;
        float bodyAttachX;
        float mainBodyWidth;
        float cannonImageWidth;
        float engineImageHeight;
        float engineImageWidth;
        float extraPartImageWidth;
        float extraPartImageHeight;
        float cannonImageHeight;
        float partOffsetX;
        float partOffsetY;
        float partAttachY;
        float bodyAttachY;
        float mainBodyHeight;
        this.setxPos(shipX);
        this.setyPos(shipY);
        PointF pnt;
        PointF rotatedPnt;
        shipX = this.getxPos();
        shipY = this.getyPos();
        //Draw Main Body
        DrawingHelper.drawImage(getMainBodyImage(), shipX, shipY, this.angle, xScale, yScale, 255);

        //Draw Extra Part
        partAttachX = extraPart.getAttachPointX() * xScale;
        bodyAttachX = mainBody.getExtraAttachX() * xScale;
        mainBodyWidth = mainBody.getImageWidth() * xScale;
        extraPartImageWidth = extraPart.getImageWidth() * xScale;
        partOffsetX = (bodyAttachX - mainBodyWidth/2) + (extraPartImageWidth/2 - partAttachX);

        partAttachY = extraPart.getAttachPointY() * yScale;
        bodyAttachY = mainBody.getExtraAttachY() * yScale;
        mainBodyHeight = mainBody.getImageHeight() * yScale;
        extraPartImageHeight = extraPart.getImageHeight() * xScale;
        partOffsetY = (bodyAttachY-mainBodyHeight/2)+(extraPartImageHeight/2 - partAttachY);
        pnt = new PointF(partOffsetX, partOffsetY);
        rotatedPnt = GraphicsUtils.rotate(pnt, GraphicsUtils.degreesToRadians((double) this.angle));
        PointF extraPartPosition = new PointF(shipX + rotatedPnt.x, shipY + rotatedPnt.y);
        DrawingHelper.drawImage(getShipExtraPartImage(), extraPartPosition.x, extraPartPosition.y, this.angle, xScale, yScale, 255);

        //Draw Cannon(right wing)
        partAttachX = cannon.getAttachPointX() * xScale;
        bodyAttachX = mainBody.getCannonAttachX() * xScale;
        cannonImageWidth = cannon.getImageWidth() * xScale;
        partOffsetX = (bodyAttachX - mainBodyWidth/2) + (cannonImageWidth/2 - partAttachX);

        partAttachY = cannon.getAttachPointY() * yScale;
        bodyAttachY = mainBody.getCannonAttachY() * yScale;
        cannonImageHeight = cannon.getImageHeight() * yScale;
        partOffsetY = (bodyAttachY-mainBodyHeight/2)+(cannonImageHeight/2 - partAttachY);
        pnt = new PointF(partOffsetX, partOffsetY);
        rotatedPnt = GraphicsUtils.rotate(pnt, GraphicsUtils.degreesToRadians((double) this.angle));
        PointF cannonPnt = new PointF(rotatedPnt.x, rotatedPnt.y);
        PointF cannonPositionOnScreen = new PointF(shipX + rotatedPnt.x, shipY + rotatedPnt.y);
        DrawingHelper.drawImage(getShipCannonImage(), cannonPositionOnScreen.x, cannonPositionOnScreen.y, this.angle, xScale, yScale, 255);
        //calculate emit position
        float emitX = cannon.getEmitPointX() * xScale - cannonImageWidth/2;
        float emitY = cannon.getEmitPointY() * yScale - cannonImageHeight/2;
        rotatedPnt = GraphicsUtils.rotate(new PointF(emitX, emitY), GraphicsUtils.degreesToRadians((double) this.angle));
        this.emitPos = new PointF(rotatedPnt.x + shipX+cannonPnt.x, rotatedPnt.y + shipY+cannonPnt.y);
        DrawingHelper.drawPoint(emitPos, 10, 65280, 255);//green

        //Draw Engine
        partAttachX = engine.getAttachPointX() * xScale;
        bodyAttachX = mainBody.getEngineAttachX() * xScale;
        engineImageWidth = engine.getImageWidth() * xScale;
        partOffsetX = (bodyAttachX - mainBodyWidth/2) + (engineImageWidth/2 - partAttachX);

        partAttachY = engine.getAttachPointY() * yScale;
        bodyAttachY = mainBody.getEngineAttachY() * yScale;
        engineImageHeight = engine.getImageHeight() * yScale;
        partOffsetY = (bodyAttachY-mainBodyHeight/2) + (engineImageHeight/2 - partAttachY);
        pnt = new PointF(partOffsetX, partOffsetY);
        rotatedPnt = GraphicsUtils.rotate(pnt, GraphicsUtils.degreesToRadians((double) this.angle));
        PointF enginePosition = new PointF(shipX + rotatedPnt.x, shipY + rotatedPnt.y);
        DrawingHelper.drawImage(getShipEngineImage(), enginePosition.x, enginePosition.y, this.angle, xScale, yScale, 255);

        this.shipWidth = mainBodyWidth + cannonImageWidth + extraPartImageWidth;
        this.shipHeight = mainBodyHeight + engineImageHeight;

    }

    public PointF calculateCannonPosition(){
        return this.emitPos;
    }

    public void setxPos(float xPos, int lvlWidth){
        if(xPos <= shipWidth/2)
            this.xPos = shipWidth/2;
        else if(xPos >= lvlWidth-shipWidth/2)
            this.xPos = lvlWidth-shipWidth/2;
        else
            this.xPos = xPos;
    }

    public void setyPos(float yPos, int lvlHeight){
        if(yPos <= shipHeight/2)
            this.yPos = shipHeight/2;
        else if(yPos >= lvlHeight-shipHeight/2)
            this.yPos = lvlHeight - shipHeight/2;
        else
            this.yPos = yPos;
    }

    public powerCores getPowerCore() {
        return powerCore;
    }

    public void setPowerCore(powerCores powerCore) {
        this.powerCore = powerCore;
    }

    public mainBodies getMainBody() {
        return mainBody;
    }

    public void setMainBody(mainBodies mainBody) {
        this.mainBody = mainBody;
    }

    public void setMainBodyImage(Integer image){
        mainBody.setConvertedImage(image);
    }

    public void setShipExtraPartImage(Integer image){
        extraPart.setConvertedImage(image);
    }

    public void setShipCannonImage(Integer image){
        cannon.setConvertedImage(image);
    }

    public void setShipCannonAttackImage(Integer image){
        cannon.setAttackImageConverted(image);
    }

    public void setCannonAttackImage(String img){
        cannon.setAttackImage(img);
    }

    public void setShipEngineImage(Integer image){
        engine.setConvertedImage(image);
    }

    public Integer getShipCannonImage(){
        return cannon.getConvertedImage();
    }

    public Integer getShipEngineImage(){
        return engine.getConvertedImage();
    }

    public Integer getMainBodyImage(){
        return  mainBody.getConvertedImage();
    }

    public Integer getShipExtraPartImage(){
        return extraPart.getConvertedImage();
    }

    public float getShipWidth() {
        return shipWidth;
    }

    public void setShipWidth(float shipWidth) {
        this.shipWidth = shipWidth;
    }

    public float getShipHeight() {
        return shipHeight;
    }

    public void setShipHeight(float shipHeight) {
        this.shipHeight = shipHeight;
    }

    public cannons getCannon() {
        return cannon;
    }

    public void setCannon(cannons cannon) {
        this.cannon = cannon;
    }

    public extraParts getExtraPart() {
        return extraPart;
    }

    public void setExtraPart(extraParts extraPart) {
        this.extraPart = extraPart;
    }

    public engines getEngine() {
        return engine;
    }

    public void setEngine(engines engine) {
        this.engine = engine;
    }

    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }
}
