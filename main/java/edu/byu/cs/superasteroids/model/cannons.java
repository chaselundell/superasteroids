package edu.byu.cs.superasteroids.model;

/**
 * Created by chaselundell on 2/11/2016.
 */
public class cannons {
    private int attachPointX;
    private int attachPointY;
    private int emitPointX;
    private int emitPointY;
    private String image;
    private int imageWidth;
    private int imageHeight;
    private String attackImage;
    private int attackImageWidth;
    private int attackImageHeight;
    private String attackSound;
    private int damage;
    private Integer convertedImage;
    private Integer attackImageConverted;

    public cannons(){
        attachPointX = 0;
        attachPointY = 0;
        emitPointX = 0;
        emitPointY = 0;
        image = "";
        imageWidth = 0;
        imageHeight = 0;
        attackImage = "";
        attackImageWidth = 0;
        attackImageHeight = 0;
        attackSound = "";
        damage = 0;
        convertedImage = 0;
        attackImageConverted = 0;

    }

    public Integer getAttackImageConverted() {
        return attackImageConverted;
    }

    public void setAttackImageConverted(Integer attackImageConverted) {
        this.attackImageConverted = attackImageConverted;
    }

    public Integer getConvertedImage() {
        return convertedImage;
    }

    public void setConvertedImage(Integer convertedImage) {
        this.convertedImage = convertedImage;
    }

    public int getAttachPointX() {
        return attachPointX;
    }

    public int getAttachPointY() {
        return attachPointY;
    }

    public void setAttachPointY(int attachPointY) {
        this.attachPointY = attachPointY;
    }

    public void setAttachPointX(int attachPointX) {
        this.attachPointX = attachPointX;
    }

    public int getEmitPointX() {
        return emitPointX;
    }

    public void setEmitPointX(int emitPointX) {
        this.emitPointX = emitPointX;
    }

    public int getEmitPointY() {
        return emitPointY;
    }

    public void setEmitPointY(int emitPointY) {
        this.emitPointY = emitPointY;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getImageWidth() {
        return imageWidth;
    }

    public void setImageWidth(int imageWidth) {
        this.imageWidth = imageWidth;
    }

    public int getImageHeight() {
        return imageHeight;
    }

    public void setImageHeight(int imageHeight) {
        this.imageHeight = imageHeight;
    }

    public String getAttackImage() {
        return attackImage;
    }

    public void setAttackImage(String attackImage) {
        this.attackImage = attackImage;
    }

    public int getAttackImageWidth() {
        return attackImageWidth;
    }

    public void setAttackImageWidth(int attackImageWidth) {
        this.attackImageWidth = attackImageWidth;
    }

    public int getAttackImageHeight() {
        return attackImageHeight;
    }

    public void setAttackImageHeight(int attackImageHeight) {
        this.attackImageHeight = attackImageHeight;
    }

    public String getAttackSound() {
        return attackSound;
    }

    public void setAttackSound(String attackSound) {
        this.attackSound = attackSound;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }
}
