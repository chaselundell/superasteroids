package edu.byu.cs.superasteroids.game;

import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import edu.byu.cs.superasteroids.base.IGameDelegate;
import edu.byu.cs.superasteroids.content.ContentManager;
import edu.byu.cs.superasteroids.core.GraphicsUtils;
import edu.byu.cs.superasteroids.drawing.DrawingHelper;
import edu.byu.cs.superasteroids.model.MainGame;
import edu.byu.cs.superasteroids.model.asteroidTypes;
import edu.byu.cs.superasteroids.model.bckGndObjectTypes;
import edu.byu.cs.superasteroids.model.bullets;
import edu.byu.cs.superasteroids.model.cannons;
import edu.byu.cs.superasteroids.model.engines;
import edu.byu.cs.superasteroids.model.extraParts;
import edu.byu.cs.superasteroids.model.levelAsteroids;
import edu.byu.cs.superasteroids.model.levelBckGndObjects;
import edu.byu.cs.superasteroids.model.levels;
import edu.byu.cs.superasteroids.model.mainBodies;
import edu.byu.cs.superasteroids.model.powerCores;
import edu.byu.cs.superasteroids.model.ship;

public class gameDelegate implements IGameDelegate {
    /**
     * Created by chaselundell on 3/1/2016.
     * This class is for the main game to play
     */
    public class AsteroidPositions {
        private int id;
        private int image;
        private List<PointF> asteroidPositions;
        private List<PointF> speeds;
        private int asteroidWidth;
        private int asteroidHeight;

        public int getAsteroidWidth() {
            return asteroidWidth;
        }

        public void setAsteroidWidth(int asteroidWidth) {
            this.asteroidWidth = asteroidWidth;
        }

        public int getAsteroidHeight() {
            return asteroidHeight;
        }

        public void setAsteroidHeight(int asteroidHeight) {
            this.asteroidHeight = asteroidHeight;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getImage() {
            return image;
        }

        public void setImage(int image) {
            this.image = image;
        }

        public List<PointF> getAsteroidPositions() {
            return asteroidPositions;
        }

        public void setAsteroidPositions(List<PointF> asteroidPositions) {
            this.asteroidPositions = asteroidPositions;
        }

        public List<PointF> getSpeed() {
            return speeds;
        }

        public void setSpeed(List<PointF> speeds) {
            this.speeds = speeds;
        }

        public AsteroidPositions(){
            asteroidPositions = new ArrayList<>();
            speeds = new ArrayList<>();
        }
    }
    private ContentManager content;
    //part objects
    private List<bckGndObjectTypes> backgroundTypes = new ArrayList<>();
    private List<levelBckGndObjects> lvlBackgroundTypes = new ArrayList<>();
    private List<levelAsteroids> lvlAsteroids = new ArrayList<>();
    private List<asteroidTypes> lvlAsteroidTypes = new ArrayList<>();
    private List<levels> lvlList = new ArrayList<>();
    private List<cannons> cannonParts = new ArrayList<>();
    private List<engines> engineParts = new ArrayList<>();
    private List<extraParts> extraPartsParts = new ArrayList<>();
    private List<mainBodies> mainBodyParts = new ArrayList<>();
    //image lists
    private Set<Integer> allImages = new HashSet<>();
    private List<Integer> cannonImages = new ArrayList<>();
    private List<Integer> cannonAttackImages = new ArrayList<>();
    private List<Integer> backgroundTypesImages = new ArrayList<>();
    private List<Integer> lvlBackgroundTypesImages = new ArrayList<>();
    private List<Integer> lvlAsteroidTypesImages = new ArrayList<>();
    private List<Integer> lvlAsteroidImages = new ArrayList<>();
    private List<Integer> mainBodyImages = new ArrayList<>();
    private List<Integer> extraPartImages = new ArrayList<>();
    private List<Integer> engineImages = new ArrayList<>();
    //scales
    private float xScale = (float) 0.2;
    private float yScale = (float) 0.2;
    //ship object
    private ship gameShip;
    private int shipAndViewportVel;
    private int bulletSound;
    //static images
    private int spaceBckGnd;
    //level information
    private int currentLvl;
    private List<AsteroidPositions> asteroidPositions = new ArrayList<>();
    private List<bullets> gameBullets = new ArrayList<>();
    levels lvl;
    //viewport information    Rect has params left, top, right, bottom
    Rect viewport = new Rect();
    Rect worldView = new Rect();
    boolean first = true;
    boolean shipInvinsible;
    int shipInvinsibleCounter = 0;
    public gameDelegate(){
        cannonParts = MainGame.getCannonsModel();
        mainBodyParts = MainGame.getMainBodiesModel();
        extraPartsParts = MainGame.getExtraPartsModel();
        engineParts = MainGame.getEnginesModel();
        gameShip = MainGame.getShipModel();
        backgroundTypes = MainGame.getBckGndObjectTypesModel();
        lvlBackgroundTypes = MainGame.getLevelBckGndObjectsModel();
        lvlAsteroidTypes = MainGame.getAsteroidTypesModel();
        lvlAsteroids = MainGame.getLevelAsteroidsModel();
        currentLvl = 0;
        lvlList = MainGame.getLevelsModel();
        lvl = new levels();
        shipAndViewportVel = 4;
    }
    public void initialize(){
        getNextLevel();
        viewport.set(lvl.getWidth() / 2, lvl.getHeight() / 2, lvl.getWidth() / 2 + DrawingHelper.getGameViewWidth(), lvl.getHeight() / 2 + DrawingHelper.getGameViewHeight());
        worldView.set(0, 0, lvl.getWidth(), lvl.getHeight());
        Random r = new Random();
        for(int i = 0; i < lvlAsteroids.size(); i++) {
            AsteroidPositions tempPositions = new AsteroidPositions();
            tempPositions.setId(lvlAsteroids.get(i).getAsteroidId()-1);
            tempPositions.setImage(lvlAsteroidImages.get(tempPositions.getId()));
            tempPositions.setAsteroidWidth(lvlAsteroidTypes.get((i%3)).getImageWidth());
            tempPositions.setAsteroidHeight(lvlAsteroidTypes.get((i%3)).getImageHeight());
            List<PointF> positionsList = new ArrayList<>();
            List<PointF> speedsList = new ArrayList<>();
            for(int j = 0; j < lvlAsteroids.get(i).getNumOfAsteroids(); j++){
                int Low = 0;
                int High = lvl.getHeight();
                int resultX = r.nextInt(High-Low) + Low;
                int resultY = r.nextInt(High-Low) + Low;
                PointF result = new PointF(resultX, resultY);
                positionsList.add(result);
                PointF vel = new PointF();
                if(resultY < lvl.getHeight()/4){
                    vel.set(1, 1);
                    speedsList.add(vel);
                }else if(resultY > lvl.getHeight()/4 && resultY < lvl.getHeight()/2){
                    vel.set(-1, 1);
                    speedsList.add(vel);
                }else if(resultY > lvl.getHeight()/2 && resultY < lvl.getHeight()/4*3){
                    vel.set(-1, -1);
                    speedsList.add(vel);
                }else if(resultY > lvl.getHeight()/4*3 && resultY < lvl.getHeight()){
                    vel.set(1, -1);
                    speedsList.add(vel);
                }
            }
            tempPositions.setSpeed(speedsList);
            tempPositions.setAsteroidPositions(positionsList);
            asteroidPositions.add(tempPositions);
            shipInvinsible = false;
        }
        gameShip.setxPos(DrawingHelper.getGameViewWidth() / 2, lvl.getWidth());
        gameShip.setyPos(DrawingHelper.getGameViewHeight() / 2, lvl.getHeight());
    }
    /**
     * Updates the game delegate. The game engine will call this function 60 times a second
     * once it enters the game loop.
     * @param elapsedTime Time since the last update. For this game, elapsedTime is always
     *                    1/60th of second
     */
    @Override
    public void update(double elapsedTime) {
        if(InputManager.movePoint != null){
            PointF moveHere = InputManager.movePoint;
            //calculations for new ship angle
            float newX;
            float newY;
            if(moveHere.x < gameShip.getxPos()) {
                newX = gameShip.getxPos() - moveHere.x;
                newY = gameShip.getyPos() - moveHere.y;
            }else{
                newX = moveHere.x- gameShip.getxPos();
                newY = moveHere.y - gameShip.getyPos();
            }
            Double angle = new Double(GraphicsUtils.radiansToDegrees(Math.atan(newY / newX)) - 90);
            float newAngle = angle.floatValue();
            if(moveHere.x > gameShip.getxPos()) {
                newAngle += 180;
            }
            gameShip.setAngle(newAngle);

            //adjust viewport towards the direction of the touch point.
            calculateAndSetViewPort(moveHere);

        }
        for(bullets bullet : gameBullets){
            bullet.setxPos(bullet.getxPos() + bullet.getBulletSpeed().x);
            bullet.setyPos(bullet.getyPos() + bullet.getBulletSpeed().y);
        }
        if(InputManager.firePressed){
            PointF shootHere = InputManager.movePoint;
            PointF cannonPos = gameShip.calculateCannonPosition();
            //set bullet positions array to cannonPos and then use bullet velocity array to move them.
            bullets bullet = new bullets();
            bullet.setxPos(cannonPos.x);
            bullet.setyPos(cannonPos.y);
            bullet.setAngle(gameShip.getAngle());
            bullet.setBulletWidth((int) (340*xScale));
            bullet.setBulletHeight((int) (340*yScale));
            Point bulletSpeed = getXYVelocity(shootHere, (int) gameShip.getxPos(), (int)  gameShip.getyPos(), 12);
            bullet.setBulletSpeed(bulletSpeed);
            gameBullets.add(bullet);
            content.playSound(bulletSound, (float) 0.8,(float) 1.0);
        }
        //update asteroid positions
        int index = 0;
        for(AsteroidPositions tempPositions : asteroidPositions){
            List<PointF> tempPositionsList = new ArrayList<>();
            tempPositionsList.addAll(tempPositions.getAsteroidPositions());

            for(PointF pos : tempPositionsList){
                int asteroidWidth = tempPositions.getAsteroidWidth();
                int asteroidHeight = tempPositions.getAsteroidHeight();
                boolean isCollision = checkForCollision(pos, asteroidWidth, asteroidHeight);
                if(isCollision){
                    tempPositions.asteroidPositions.remove(index);
                    tempPositions.speeds.remove(index);
                }else {
                    PointF speed = tempPositions.speeds.get(index);
                    PointF negSpeed = new PointF(speed.x, speed.y);
                    negSpeed.negate();
                    if (pos.x > 0 && pos.x < lvl.getWidth()) {
                        pos.x += speed.x;
                    } else {
                        pos.x += negSpeed.x;
                        speed.x = negSpeed.x;
                    }
                    if (pos.y > 0 && pos.y < lvl.getHeight()) {
                        pos.y += speed.y;
                    } else {
                        pos.y += negSpeed.y;
                        speed.y = negSpeed.y;
                    }
                    index++;
                }

            }
            index = 0;
        }
    }
    //checks asteroid against the ship position and then against the bullet pos
    //if it intersects the ship, put ship invinsible flag on for 3 seconds, if it intersects
    //the bullet, remove bullet from list of positions and velocities
    private boolean checkForCollision(PointF asteroidPos, int astWidth, int astHeight){
        Rect asteroidBox = new Rect((int) asteroidPos.x,(int)  asteroidPos.y, ((int) asteroidPos.x)+astWidth/2, ((int)  asteroidPos.y)+astHeight/2);
        int shipLeft = (int) gameShip.getxPos()+viewport.left;
        int shipTop = (int)  gameShip.getyPos()+viewport.top;
        int shipRight = (int)  (gameShip.getxPos() + gameShip.getShipWidth()/2) + viewport.left;
        int shipBottom = (int) (gameShip.getyPos()+gameShip.getShipWidth()/2) + viewport.top;
        Rect shipBox = new Rect(shipLeft,shipTop,shipRight,shipBottom);

        boolean shipIntersect = false;
        if(shipBox.intersect(asteroidBox)){
            shipInvinsible = true;
            shipIntersect = true;
        }
        int index = 0;
        for(bullets bullet : gameBullets){
            int bulletLeft = (int) bullet.getxPos()+ viewport.left;
            int bulletTop = (int)  bullet.getyPos()+viewport.top;
            int bulletRight = ((int)  bullet.getxPos() + bullet.getBulletWidth()) + viewport.left;
            int bulletBottom = ((int)  bullet.getyPos())+bullet.getBulletHeight() + viewport.top;
            Rect bulletBox = new Rect(bulletLeft,bulletTop,bulletRight,bulletBottom);
            if(asteroidBox.intersect(bulletBox)){
                gameBullets.remove(index);
                return true;
            }
            index++;
        }
        return shipIntersect;
    }

    private void calculateAndSetViewPort(PointF moveHere){
        int middleX = DrawingHelper.getGameViewWidth()/2;
        int middleY = DrawingHelper.getGameViewHeight()/2;
        int shipX = (int) gameShip.getxPos();
        int shipY = (int) gameShip.getyPos();
        int newLeft = viewport.left;
        int newTop = viewport.top;
        int newRight = viewport.right;
        int newBottom = viewport.bottom;
        boolean onLeft = (viewport.left <= 0 && shipX <= middleX);
        boolean onTop = (viewport.top <= 0 && shipY <= middleY);
        boolean onRight = (viewport.right >= lvl.getWidth() && shipX >= middleX);
        boolean onBottom = (viewport.bottom >= lvl.getHeight() && shipY >= middleY);
        Point xyVel = getXYVelocity(moveHere, shipX, shipY, shipAndViewportVel);
        //ship is on left side of middle as viewport is on left side of screen and not touching top or bottom
        if((onLeft && !onTop && !onBottom) || (onLeft && !onBottom && !onTop) ||
                (onRight && !onTop && !onBottom) || (onRight && !onBottom && !onTop)){
            gameShip.setxPos((float) (shipX + xyVel.x), lvl.getWidth());
            newTop += xyVel.y;
            newBottom += xyVel.y;
        }else if(onLeft && onTop ||
                onTop && onRight ||
                onBottom && onRight ||
                onBottom && onLeft
                ){
            gameShip.setxPos((float) (shipX + xyVel.x), lvl.getWidth());
            gameShip.setyPos((float) (shipY + xyVel.y), lvl.getHeight());
        }else if((onTop && !onLeft && !onRight) || (onTop && !onRight && !onLeft) ||
                (onBottom && !onLeft && !onRight) || (onBottom && !onRight && !onLeft)){
            gameShip.setyPos((float) (shipY + xyVel.y), lvl.getHeight());
            newLeft += xyVel.x;
            newRight += xyVel.x;
        }else{
            newLeft += xyVel.x;
            newRight += xyVel.x;
            newTop += xyVel.y;
            newBottom += xyVel.y;
            gameShip.setxPos(middleX, lvl.getWidth());
            gameShip.setyPos(middleY, lvl.getHeight());
        }
        if(newLeft < 0) {
            newLeft = 0;
        }
        if(newTop < 0) {
            newTop = 0;
        }
        if(newRight > lvl.getWidth()) {
            newRight = lvl.getWidth();
        }
        if(newBottom > lvl.getHeight()) {
            newBottom = lvl.getHeight();
        }
        viewport.set(newLeft, newTop, newRight, newBottom);//left top right bottom
    }

    /**
     * This function will take a touch point and the ship position and return to you the velocity
     * the ship should move as a point object with attributes dx, dy (velocity x, velocity y)
     */
    private Point getXYVelocity(PointF moveHere, int shipX, int shipY, int speed){
        PointF toMoveHere = new PointF((int) moveHere.x,(int) moveHere.y);
        Point velocity = new Point(0,0);
        PointF shipPnt = new PointF(shipX, shipY);
        int touchX = (int) toMoveHere.x;
        int touchY = (int) toMoveHere.y;
        PointF velPnt;
        velPnt = GraphicsUtils.subtract(toMoveHere, shipPnt);
        velPnt.set(velPnt.x/(DrawingHelper.getGameViewWidth()/4) * speed, velPnt.y/(DrawingHelper.getGameViewHeight()/4) * speed);
        if(touchX > shipX && touchY > shipY){//Top Right touch point (from ship)
            velocity.offset((int) velPnt.x, (int) velPnt.y);
        }
        if(touchX > shipX && touchY < shipY){//Bottom Right touch point (from ship)
            velocity.offset((int) velPnt.x, (int) velPnt.y);
        }
        if(touchX < shipX && touchY > shipY){//Top Left touch point (from ship)
            velocity.offset((int) velPnt.x, (int) velPnt.y);
        }
        if(touchX < shipX && touchY < shipY){//Bottom Left touch point (from ship)
            velocity.offset((int) velPnt.x, (int) velPnt.y);
        }
        return velocity;
    }
    /**
     * Draws the game delegate. This function will be 60 times a second.
     */
    @Override
    public void draw() {

        if(first){
            initialize();
            first = false;
        }
        //Draw Background
        //dest is in view coordinates(specify where to draw portion of image)
        Rect dest = new Rect(0, 0, DrawingHelper.getGameViewWidth(), DrawingHelper.getGameViewHeight());
        //src is in image coordinates(specifies portion of image to draw)
        DrawingHelper.drawImage(spaceBckGnd, viewport, dest);

        //Draw Background Objects
        for(levelBckGndObjects lvlBackgrounds : lvlBackgroundTypes){
            PointF pos = new PointF(lvlBackgrounds.getPositionX(), lvlBackgrounds.getPositionY());
            pos = calculateWorldToViewportCoordinates(pos);
            int imageId = backgroundTypesImages.get(lvlBackgrounds.getLvlBckGndObjectId()-1);
            lvlBackgrounds.drawImage(imageId, pos.x, pos.y, 0, (float) 0.4, (float) 0.4, 255);
        }

        //Draw Asteroids
        for(AsteroidPositions tempPosition : asteroidPositions){
            for(PointF position : tempPosition.getAsteroidPositions()){
                position = calculateWorldToViewportCoordinates(position);
                DrawingHelper.drawImage(tempPosition.getImage(), position.x, position.y, 0, (float) 0.4, (float) 0.4, 255);
            }
        }

        //Draw Bullets
        for(bullets bullet : gameBullets) {
            gameShip.drawBullets(bullet.getxPos(), bullet.getyPos(), bullet.getAngle(), xScale, yScale);
        }

        //Draw Ship
        if(shipInvinsible){
            DrawingHelper.drawFilledCircle(new PointF(gameShip.getxPos(), gameShip.getyPos()),gameShip.getShipWidth()/2+30, 5263440, 175);
            shipInvinsibleCounter++;
            if(shipInvinsibleCounter == 180){//3 sec
                shipInvinsible = false;
                shipInvinsibleCounter = 0;
            }
        }else{
            DrawingHelper.drawFilledCircle(new PointF(gameShip.getxPos(), gameShip.getyPos()),gameShip.getShipWidth(), 0, 0);
        }
            gameShip.drawShip(gameShip.getxPos(), gameShip.getyPos(), xScale, yScale);

        //draw minimap
        int minimapWidth = 200;
        int minimapLeft = 0;
        int minimapRight = DrawingHelper.getGameViewWidth();
        int minimapTop = 0;
        int minimapBottom = minimapWidth;
        if(gameShip.getxPos() + gameShip.getShipWidth() > DrawingHelper.getGameViewWidth()-minimapWidth){
            minimapRight = minimapWidth;
        }else{
            minimapLeft = DrawingHelper.getGameViewWidth()-minimapWidth;
        }
        Rect minimap = new Rect(minimapLeft, minimapTop,minimapRight, minimapBottom);
        DrawingHelper.drawFilledRectangle(minimap, 5263440, 175);//gray

        Rect minimapsMinimap = new Rect(minimapLeft + viewport.left/15, viewport.top/15, minimapLeft + viewport.right/15, viewport.bottom/15);
        DrawingHelper.drawFilledRectangle(minimapsMinimap, 8421504, 175);//light gray

        PointF shipDot = calculateWorldToMinimapCoordinates(new PointF(gameShip.getxPos()+viewport.left, gameShip.getyPos()+viewport.top));
        shipDot.set(shipDot.x + minimapLeft, shipDot.y);
        DrawingHelper.drawPoint(shipDot, 5, 65280, 255);//green

        for(AsteroidPositions tempPosition : asteroidPositions){
            for(PointF position : tempPosition.getAsteroidPositions()){
                PointF asteroidMinimapPos = calculateWorldToMinimapCoordinates(position);
                asteroidMinimapPos.set(asteroidMinimapPos.x + minimapLeft, asteroidMinimapPos.y);
                DrawingHelper.drawPoint(asteroidMinimapPos, 5, 16711680, 255);//red
            }
        }

        //for testing
//        Point p = new Point((int) gameShip.getxPos()+160, (int)gameShip.getyPos());
//        String text = "shipX = " + gameShip.getxPos() + "\n" +
//                      "shipY = " + gameShip.getyPos() + "\n" +
//                      "viewPortLeft = " + viewport.left + "\n" +
//                      "viewPortTop = " + viewport.top + "\n";
//
//        String line2 = "viewPortRight = " + viewport.right + "\n" +
//                "viewPortBottom = " + viewport.bottom + "\n";
//        DrawingHelper.drawText(p, text, 65280, 12);//green
//        p.offset(0, 15);
//        DrawingHelper.drawText(p, line2, 65280, 12);//green
    }

    private PointF calculateWorldToMinimapCoordinates(PointF worldCoordinates){
        PointF minimapPnt = new PointF(worldCoordinates.x/15, worldCoordinates.y/15);
        return minimapPnt;
    }

    private PointF calculateWorldToViewportCoordinates(PointF worldCoordinates){
        return new PointF(worldCoordinates.x - viewport.left, worldCoordinates.y - viewport.top);
    }

    private PointF calculateViewportToWorldCoordinates(PointF viewportCoordinates){
        return new PointF(viewportCoordinates.x + viewport.left, viewportCoordinates.y + viewport.top);
    }

    private void getNextLevel(){
        this.lvl = lvlList.get(currentLvl);
        currentLvl++;
    }
    /**
     * Loads content such as image and sounds files and other data into the game. The GameActivty will
     * call this once right before entering the game engine enters the game loop. The ShipBuilding
     * activity calls this function in its onCreate() function.
     * @param contentToLoad An instance of the content manager. This should be used to load images and sound
     *                files.
     */
    @Override
    public void loadContent(ContentManager contentToLoad) {
        if(content ==  null && MainGame.getCannonsModel() != null) {
            content = contentToLoad;

            spaceBckGnd = content.loadImage("images/space.bmp");
            try {
                bulletSound = content.loadSound(gameShip.getCannon().getAttackSound());
            } catch (IOException e) {
                //bad place to be!
            }
            for (cannons cannon : cannonParts) {
                int imageId = content.loadImage(cannon.getImage());
                cannonImages.add(imageId);
                allImages.add(imageId);
                imageId = content.loadImage(cannon.getAttackImage());
                cannonAttackImages.add(imageId);
                gameShip.setShipCannonAttackImage(imageId);
                allImages.add(imageId);
            }

            for(asteroidTypes lvlAsteroidType : lvlAsteroidTypes){
                int imageId = content.loadImage(lvlAsteroidType.getImage());
                lvlAsteroidTypesImages.add(imageId);
                allImages.add(imageId);
            }

            for(levelAsteroids lvlAsteroid: lvlAsteroids){
                int imageId = lvlAsteroidTypesImages.get(lvlAsteroid.getAsteroidId()-1);
                lvlAsteroidImages.add(imageId);
            }

            for(bckGndObjectTypes type : backgroundTypes){
                int imageId = content.loadImage(type.getImgPath());
                backgroundTypesImages.add(imageId);
                allImages.add(imageId);
            }

            for(levelBckGndObjects lvlBackgrounds: lvlBackgroundTypes){
                int imageId = backgroundTypesImages.get(lvlBackgrounds.getLvlBckGndObjectId()-1);
                lvlBackgroundTypesImages.add(imageId);
            }

            //have to do this to load images for quick play
            for (engines engine : engineParts) {
                int imageId = content.loadImage(engine.getImage());
                engineImages.add(imageId);
            }

            for (extraParts extraPart : extraPartsParts) {
                int imageId = content.loadImage(extraPart.getImage());
                extraPartImages.add(imageId);
            }

            for (mainBodies mainBody : mainBodyParts) {
                int imageId = content.loadImage(mainBody.getImage());
                mainBodyImages.add(imageId);
            }
            if(MainGame.getQuickPlayChosenIndex() != -1) {
                MainGame.getShipModel().setShipCannonImage(cannonImages.get(MainGame.getQuickPlayChosenIndex()));
                MainGame.getShipModel().setShipCannonAttackImage(cannonAttackImages.get(MainGame.getQuickPlayChosenIndex()));
                MainGame.getShipModel().setShipEngineImage(engineImages.get(MainGame.getQuickPlayChosenIndex()));
                MainGame.getShipModel().setShipExtraPartImage(extraPartImages.get(MainGame.getQuickPlayChosenIndex()));
                MainGame.getShipModel().setMainBodyImage(mainBodyImages.get(MainGame.getQuickPlayChosenIndex()));
            }
        }
    }

    /**
     * Unloads content from the game. The GameActivity will call this function after the game engine
     * exits the game loop. The ShipBuildingActivity will call this function after the "Start Game"
     * button has been pressed.
     * @param content An instance of the content manager. This should be used to unload image and
     *                sound files.
     */
    @Override
    public void unloadContent(ContentManager content) {

    }
}
