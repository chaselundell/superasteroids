package edu.byu.cs.superasteroids.ship_builder;

import android.graphics.Point;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.byu.cs.superasteroids.base.IView;
import edu.byu.cs.superasteroids.content.ContentManager;
import edu.byu.cs.superasteroids.drawing.DrawingHelper;
import edu.byu.cs.superasteroids.model.MainGame;
import edu.byu.cs.superasteroids.model.cannons;
import edu.byu.cs.superasteroids.model.engines;
import edu.byu.cs.superasteroids.model.extraParts;
import edu.byu.cs.superasteroids.model.mainBodies;
import edu.byu.cs.superasteroids.model.powerCores;
import edu.byu.cs.superasteroids.model.ship;

/**
 * Created by chaselundell on 2/24/2016.
 * This is the controller to help the user build their ship.
 */
public class ShipBuilderController implements IShipBuildingController {
    private ContentManager content;
    private Set<Integer> allImages = new HashSet<>();
    IShipBuildingView view;
    IShipBuildingView.PartSelectionView partView;
    IShipBuildingView.ViewDirection direction;
    private List<Integer> mainBodyImages = new ArrayList<>();
    private List<Integer> cannonImages = new ArrayList<>();
    private List<Integer> extraPartImages = new ArrayList<>();
    private List<Integer> engineImages = new ArrayList<>();
    private List<Integer> powerCoreImages = new ArrayList<>();
    private String shipBuildingState = "MainBody";
    private String shipState = null;
    private int mainBodyIndex = -1;
    private int cannonIndex = -1;
    private int extraPartIndex = -1;
    private int engineIndex = -1;
    private int powerCoreIndex = -1;
    private int partsSelected = 0;
    private List<cannons> cannonParts = new ArrayList<>();
    private List<engines> engineParts = new ArrayList<>();
    private List<extraParts> extraPartsParts = new ArrayList<>();
    private List<powerCores> powerCoreParts = new ArrayList<>();
    private List<mainBodies> mainBodyParts = new ArrayList<>();
    private ship gameShip = MainGame.getShipModel();
    private String extra_parts = "Extra Parts";
    private String power_cores = "Power Cores";
    private String engines_str = "Engines";
    private String cannons_str = "Cannons";
    private String main_body_str = "Main Body";
    private float xScale = (float) 0.3;
    private float yScale = (float) 0.3;

    public ShipBuilderController(IShipBuildingView view){
        this.view = view;
        mainBodyParts = MainGame.getMainBodiesModel();
        powerCoreParts = MainGame.getPowerCoresModel();
        extraPartsParts = MainGame.getExtraPartsModel();
        engineParts = MainGame.getEnginesModel();
        cannonParts = MainGame.getCannonsModel();
    }
    @Override
    public void onViewLoaded(IShipBuildingView.PartSelectionView partViewParam) {
        boolean visible = true;
        partView = partViewParam;
        switch (partViewParam){
            case MAIN_BODY:
                shipBuildingState = "MainBody";
                view.setArrow(partView.MAIN_BODY, direction.LEFT, visible, extra_parts);
                view.setArrow(partView.MAIN_BODY, direction.UP, visible, power_cores);
                view.setArrow(partView.MAIN_BODY, direction.DOWN, visible, engines_str);
                view.setArrow(partView.MAIN_BODY, direction.RIGHT, visible, cannons_str);
                break;
            case EXTRA_PART:
                shipBuildingState = "ExtraPart";
                view.setArrow(partView.EXTRA_PART, direction.RIGHT, visible, main_body_str);
                break;
            case CANNON:
                shipBuildingState = "CANNON";
                view.setArrow(partView.CANNON, direction.LEFT, visible, main_body_str);
                break;
            case ENGINE:
                shipBuildingState = "ENGINE";
                view.setArrow(partView.ENGINE, direction.UP, visible, main_body_str);
                break;
            case POWER_CORE:
                shipBuildingState = "POWER_CORE";
                view.setArrow(partView.POWER_CORE, direction.DOWN, visible, main_body_str);
                break;
        }
        if(partsSelected >= 5){
            MainGame.setShipModel(gameShip);
            view.setStartGameButton(true);
        }

    }

    @Override
    public void update(double elapsedTime) {
        //nothing
    }

    @Override
    public void loadContent(ContentManager contentToLoad) {
        if(content ==  null && MainGame.getCannonsModel() != null) {
            content = contentToLoad;
            for (cannons cannon : cannonParts) {
                int imageId = content.loadImage(cannon.getImage());
                cannonImages.add(imageId);
                allImages.add(imageId);
            }
            view.setPartViewImageList(partView.CANNON, cannonImages);

            for (engines engine : engineParts) {
                int imageId = content.loadImage(engine.getImage());
                engineImages.add(imageId);
                allImages.add(imageId);
            }
            view.setPartViewImageList(partView.ENGINE, engineImages);

            for (extraParts extraPart : extraPartsParts) {
                int imageId = content.loadImage(extraPart.getImage());
                extraPartImages.add(imageId);
                allImages.add(imageId);
            }
            view.setPartViewImageList(partView.EXTRA_PART, extraPartImages);

            for (powerCores powerCore : powerCoreParts) {
                int imageId = content.loadImage(powerCore.getImage());
                powerCoreImages.add(imageId);
                allImages.add(imageId);
            }
            view.setPartViewImageList(partView.POWER_CORE, powerCoreImages);

            for (mainBodies mainBody : mainBodyParts) {
                int imageId = content.loadImage(mainBody.getImage());
                mainBodyImages.add(imageId);
                allImages.add(imageId);
            }
            view.setPartViewImageList(partView.MAIN_BODY, mainBodyImages);
        }
    }

    @Override
    public void unloadContent(ContentManager contentToUnLoad) {
        //nothing
    }

    @Override
    public void draw() {
        Point gameViewCenter = new Point(DrawingHelper.getGameViewWidth() / 2,DrawingHelper.getGameViewHeight() / 2);
        float yOffset = 40;
        if(mainBodyIndex != -1){
            MainGame.getShipModel().setMainBodyImage(mainBodyImages.get(mainBodyIndex));
            DrawingHelper.drawImage(mainBodyImages.get(mainBodyIndex), gameViewCenter.x, gameViewCenter.y-yOffset , 0, xScale, yScale, 175);
        }
        if(cannonIndex != -1){
            //For X
            float partAttachX = MainGame.getShipModel().getCannon().getAttachPointX() * xScale;
            float bodyAttachX = MainGame.getShipModel().getMainBody().getCannonAttachX() * xScale;
            float mainBodyWidth = MainGame.getShipModel().getMainBody().getImageWidth() * xScale;
            float cannonImageWidth = MainGame.getShipModel().getCannon().getImageWidth() * xScale;
            float partLocationX = gameViewCenter.x + (Math.abs(bodyAttachX - mainBodyWidth/2)) + (Math.abs(cannonImageWidth/2 - partAttachX));
            //FOR Y
            float partAttachY = MainGame.getShipModel().getCannon().getAttachPointY() * yScale;
            float bodyAttachY = MainGame.getShipModel().getMainBody().getCannonAttachY() * yScale;
            float mainBodyHeight = MainGame.getShipModel().getMainBody().getImageHeight() * yScale;
            float cannonImageHeight = MainGame.getShipModel().getCannon().getImageHeight() * yScale;
            float partLocationY = (gameViewCenter.y-yOffset) + (bodyAttachY-mainBodyHeight/2)+(Math.abs(partAttachY-cannonImageHeight/2));
            MainGame.getShipModel().setShipCannonImage(cannonImages.get(cannonIndex));
            MainGame.getShipModel().setCannonAttackImage(cannonParts.get(cannonIndex).getAttackImage());
            DrawingHelper.drawImage(cannonImages.get(cannonIndex), partLocationX, partLocationY, 0, xScale, yScale, 175);
        }
        if(extraPartIndex != -1){
            //FOR X
            float partAttachX = MainGame.getShipModel().getExtraPart().getAttachPointX() * xScale;
            float bodyAttachX = MainGame.getShipModel().getMainBody().getExtraAttachX() * xScale;
            float mainBodyWidth = MainGame.getShipModel().getMainBody().getImageWidth() * xScale;
            float extraPartImageWidth = MainGame.getShipModel().getExtraPart().getImageWidth() * xScale;
            float partLocationX = gameViewCenter.x - (mainBodyWidth/2 - bodyAttachX)-(partAttachX - extraPartImageWidth/2);
            //FOR Y
            float partAttachY = MainGame.getShipModel().getExtraPart().getAttachPointY() * yScale;
            float bodyAttachY = MainGame.getShipModel().getMainBody().getExtraAttachY() * yScale;
            float mainBodyHeight = MainGame.getShipModel().getMainBody().getImageHeight() * yScale;
            float extraPartImageHeight = MainGame.getShipModel().getExtraPart().getImageHeight() * yScale;
            float partLocationY = (gameViewCenter.y-yOffset) +(bodyAttachY-mainBodyHeight/2)+(Math.abs(partAttachY - extraPartImageHeight / 2));
            MainGame.getShipModel().setShipExtraPartImage(extraPartImages.get(extraPartIndex));
            DrawingHelper.drawImage(extraPartImages.get(extraPartIndex), (partLocationX), (partLocationY), 0, xScale, yScale, 175);
        }
        if(engineIndex != -1){
            float partAttachY = MainGame.getShipModel().getEngine().getAttachPointY() * yScale;
            float bodyAttachY = MainGame.getShipModel().getMainBody().getEngineAttachY() * yScale;
            float mainBodyHeight = MainGame.getShipModel().getMainBody().getImageHeight() * yScale;
            float engineImageHeight = MainGame.getShipModel().getEngine().getImageHeight() * yScale;
            float partLocationY = (gameViewCenter.y-yOffset) + (Math.abs(partAttachY-engineImageHeight/2)) + (bodyAttachY-mainBodyHeight/2);
            MainGame.getShipModel().setShipEngineImage(engineImages.get(engineIndex));
            DrawingHelper.drawImage(engineImages.get(engineIndex), gameViewCenter.x, partLocationY, 0, xScale, yScale, 175);
        }
    }

    @Override
    public void onSlideView(IShipBuildingView.ViewDirection directionParam) {
        switch (partView){
            case MAIN_BODY:
                switch (directionParam){
                    case LEFT:
                        view.animateToView(partView.CANNON, directionParam.RIGHT);
                        break;
                    case RIGHT:
                        view.animateToView(partView.EXTRA_PART, directionParam.LEFT);
                        break;
                    case UP:
                        view.animateToView(partView.ENGINE, directionParam.DOWN);
                        break;
                    case DOWN:
                        view.animateToView(partView.POWER_CORE, directionParam.UP);
                        break;
                }
                break;
            case EXTRA_PART:
                if(direction.LEFT == directionParam){
                    view.animateToView(partView.MAIN_BODY, directionParam.RIGHT);
                }
                break;
            case CANNON:
                if(direction.RIGHT == directionParam){
                    view.animateToView(partView.MAIN_BODY, directionParam.LEFT);
                }
                break;
            case ENGINE:
                if(direction.DOWN == directionParam){
                    view.animateToView(partView.MAIN_BODY, directionParam.UP);
                }
                break;
            case POWER_CORE:
                if(direction.UP == directionParam){
                    view.animateToView(partView.MAIN_BODY, directionParam.DOWN);
                }
                break;
        }
    }

    @Override
    public void onPartSelected(int index) {

        shipState = shipBuildingState;
        switch (shipBuildingState){
            case "MainBody":
                gameShip.setMainBody(mainBodyParts.get(index));
                mainBodyIndex = index;
                partsSelected++;
                break;
            case "ExtraPart":
                gameShip.setExtraPart(extraPartsParts.get(index));
                extraPartIndex = index;
                partsSelected++;
                view.animateToView(partView.MAIN_BODY, direction.RIGHT);
                break;
            case "CANNON":
                gameShip.setCannon(cannonParts.get(index));
                cannonIndex = index;
                partsSelected++;
                view.animateToView(partView.MAIN_BODY, direction.LEFT);
                break;
            case "ENGINE":
                gameShip.setEngine(engineParts.get(index));
                engineIndex = index;
                partsSelected++;
                view.animateToView(partView.MAIN_BODY, direction.UP);
                break;
            case "POWER_CORE":
                gameShip.setPowerCore(powerCoreParts.get(index));
                powerCoreIndex = index;
                partsSelected++;
                view.animateToView(partView.MAIN_BODY, direction.DOWN);
                break;
        }

    }

    @Override
    public void onStartGamePressed() {
     view.startGame();
    }

    @Override
    public void onResume() {
        //nothing
    }

    @Override
    public IView getView() {
        return view;
    }

    @Override
    public void setView(IView view) {
        //nothing
    }
}
