package edu.byu.cs.superasteroids.model;

/**
 * Created by chaselundell on 2/20/2016.
 */
public class movingObjects extends visibleObjects{

    private int velocity;
    private int direction;

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public int getVelocity() {
        return velocity;
    }

    public void setVelocity(int velocity) {
        this.velocity = velocity;
    }

    public movingObjects(){
        velocity = 0;
        direction = 0;
    }
}
