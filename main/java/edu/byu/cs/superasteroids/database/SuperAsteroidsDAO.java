package edu.byu.cs.superasteroids.database;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.byu.cs.superasteroids.model.*;

/**
 * Created by chaselundell on 2/11/2016.
 * This DAO will store all information from JSON into the SQL tables and also
 * get all the information from those tables and store them in the models.
 */
public class SuperAsteroidsDAO {
    public static final SuperAsteroidsDAO SINGLETON = new SuperAsteroidsDAO();

    private static SQLiteDatabase db;
    private static final String SELECT_ALL_BACKGNDOBJECTTYPES = "select * from BACKGNDOBJECTTYPES";
    private static final String SELECT_ALL_ASTEROIDTYPES = "select * from ASTEROIDTYPES";
    private static final String SELECT_ALL_LEVELS = "select * from LEVELS";
    private static final String SELECT_ALL_LEVELASTEROIDS = "select * from LEVELASTEROIDS";
    private static final String SELECT_ALL_LEVELBACKGNDOBJECTS = "select * from LEVELBACKGNDOBJECTS";
    private static final String SELECT_ALL_MAINBODIES = "select * from MAINBODIES";
    private static final String SELECT_ALL_POWERCORES = "select * from POWERCORES";
    private static final String SELECT_ALL_CANNONS = "select * from CANNONS";
    private static final String SELECT_ALL_ENGINES = "select * from ENGINES";
    private static final String SELECT_ALL_EXTRAPARTS = "select * from EXTRAPARTS";
    private static final String[] EMPTY_ARRAY_OF_STRINGS = {};

    public SuperAsteroidsDAO() {db = null;}
    public SuperAsteroidsDAO(SQLiteDatabase db) {
        SuperAsteroidsDAO.db = db;
    }

    /**populate tables with JSON data
     *
     */

    public static boolean storeBckGndObjectTypes(bckGndObjectTypes types){
            ContentValues values = new ContentValues();
            values.put("bckGndObjectId", types.getBckGndObjectId());
            values.put("imgPath", types.getImgPath());

            long id = db.insert("BACKGNDOBJECTTYPES", null, values);

            boolean result = false;
            if (id >= 0) {
                result = true;
            }
            return result;
    }
    public static boolean storeAsteroidTypes(asteroidTypes types){
        ContentValues values = new ContentValues();
        values.put("id", types.getId());
        values.put("name", types.getName());
        values.put("image", types.getImage());
        values.put("imageWidth", types.getImageWidth());
        values.put("imageHeight", types.getImageHeight());
        values.put("type", types.getType());


        long id = db.insert("ASTEROIDTYPES", null, values);

        boolean result = false;
        if (id >= 0) {
            result = true;
        }
        return result;
    }
    public static boolean storeLevels(levels lvls){
        ContentValues values = new ContentValues();
        values.put("levelId", lvls.getLevelId());
        values.put("number", lvls.getNumber());
        values.put("title", lvls.getTitle());
        values.put("hint", lvls.getHint());
        values.put("width", lvls.getWidth());
        values.put("height", lvls.getHeight());
        values.put("music", lvls.getMusic());

        long id = db.insert("LEVELS", null, values);

        boolean result = false;
        if (id >= 0) {
            result = true;
        }
        return result;
    }
    public static boolean storeLevelAsteroids(levelAsteroids lvlAsteriods){
        ContentValues values = new ContentValues();
        values.put("levelNum", lvlAsteriods.getLevelNum());
        values.put("numOfAsteroids", lvlAsteriods.getNumOfAsteroids());
        values.put("asteroidId", lvlAsteriods.getAsteroidId());

        long id = db.insert("LEVELASTEROIDS", null, values);

        boolean result = false;
        if (id >= 0) {
            result = true;
        }
        return result;
    }
    public static boolean storeLevelBckGndObjects(levelBckGndObjects lvlBackgroundObjects){
        ContentValues values = new ContentValues();
        values.put("positionX", lvlBackgroundObjects.getPositionX());
        values.put("positionY", lvlBackgroundObjects.getPositionY());
        values.put("lvlBckGndObjectId", lvlBackgroundObjects.getLvlBckGndObjectId());
        values.put("scale", lvlBackgroundObjects.getScale());
        values.put("lvlId", lvlBackgroundObjects.getLvlId());

        long id = db.insert("LEVELBACKGNDOBJECTS", null, values);

        boolean result = false;
        if (id >= 0) {
            result = true;
        }
        return result;
    }
    public static boolean storeMainBodies(mainBodies mBodies){
        ContentValues values = new ContentValues();
        values.put("cannonAttachX", mBodies.getCannonAttachX());
        values.put("cannonAttachY", mBodies.getCannonAttachY());
        values.put("engineAttachX", mBodies.getEngineAttachX());
        values.put("engineAttachY", mBodies.getEngineAttachY());
        values.put("extraAttachX", mBodies.getExtraAttachX());
        values.put("extraAttachY", mBodies.getExtraAttachY());
        values.put("image", mBodies.getImage());
        values.put("imageWidth", mBodies.getImageWidth());
        values.put("imageHeight", mBodies.getImageHeight());

        long id = db.insert("MAINBODIES", null, values);

        boolean result = false;
        if (id >= 0) {
            result = true;
        }
        return result;
    }
    public static boolean storePowerCores(powerCores pCores){
        ContentValues values = new ContentValues();
        values.put("cannonBoost", pCores.getCannonBoost());
        values.put("engineBoost", pCores.getEngineBoost());
        values.put("image", pCores.getImage());

        long id = db.insert("POWERCORES", null, values);

        boolean result = false;
        if (id >= 0) {
            result = true;
        }
        return result;
    }
    public static boolean storeCannons(cannons can){
        ContentValues values = new ContentValues();
        values.put("attachPointX", can.getAttachPointX());
        values.put("attachPointY", can.getAttachPointY());
        values.put("emitPointX", can.getEmitPointX());
        values.put("emitPointY", can.getEmitPointY());
        values.put("image", can.getImage());
        values.put("imageWidth", can.getImageWidth());
        values.put("imageHeight", can.getImageHeight());
        values.put("attackImage", can.getAttackImage());
        values.put("attackImageWidth", can.getAttackImageWidth());
        values.put("attackImageHeight", can.getAttackImageHeight());
        values.put("attackSound", can.getAttackSound());
        values.put("damage", can.getDamage());
        long id = db.insert("CANNONS", null, values);

        boolean result = false;
        if (id >= 0) {
            result = true;
        }
        return result;
    }
    public static boolean storeEngines(engines eng){
        ContentValues values = new ContentValues();
        values.put("baseSpeed", eng.getBaseSpeed());
        values.put("baseTurnRate", eng.getBaseTurnRate());
        values.put("attachPointX", eng.getAttachPointX());
        values.put("attachPointY", eng.getAttachPointY());
        values.put("image", eng.getImage());
        values.put("imageWidth", eng.getImageWidth());
        values.put("imageHeight", eng.getImageHeight());
        long id = db.insert("ENGINES", null, values);

        boolean result = false;
        if (id >= 0) {
            result = true;
        }
        return result;
    }
    public static boolean storeExtraParts(extraParts eParts){
        ContentValues values = new ContentValues();
        values.put("attachPointX", eParts.getAttachPointX());
        values.put("attachPointY", eParts.getAttachPointY());
        values.put("image", eParts.getImage());
        values.put("imageWidth", eParts.getImageWidth());
        values.put("imageHeight", eParts.getImageHeight());
        long id = db.insert("EXTRAPARTS", null, values);

        boolean result = false;
        if (id >= 0) {
            result = true;
        }
        return result;
    }

    /**used to populate models from database data
     *
     */

    public List<bckGndObjectTypes> getBckGndObjectTypes(){
        List<bckGndObjectTypes> result = new ArrayList<>();
        Cursor cursor = db.rawQuery(SELECT_ALL_BACKGNDOBJECTTYPES, EMPTY_ARRAY_OF_STRINGS);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                bckGndObjectTypes temp = new bckGndObjectTypes();
                temp.setBckGndObjectId(cursor.getInt(0));
                temp.setImgPath(cursor.getString(1));
                result.add(temp);
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }

        return result;
    }

    public List<asteroidTypes> getAsteroidTypes(){
        List<asteroidTypes> result = new ArrayList<>();
        Cursor cursor = db.rawQuery(SELECT_ALL_ASTEROIDTYPES, EMPTY_ARRAY_OF_STRINGS);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                asteroidTypes temp = new asteroidTypes();
                temp.setId(cursor.getInt(0));
                temp.setName(cursor.getString(1));
                temp.setImage(cursor.getString(2));
                temp.setImageWidth(cursor.getInt(3));
                temp.setImageHeight(cursor.getInt(4));
                temp.setType(cursor.getString(5));
                result.add(temp);
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }

        return result;
    }

    public List<cannons> getCannons(){
        List<cannons> result = new ArrayList<>();
        Cursor cursor = db.rawQuery(SELECT_ALL_CANNONS, EMPTY_ARRAY_OF_STRINGS);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                cannons temp = new cannons();
                temp.setAttachPointX(cursor.getInt(0));
                temp.setAttachPointY(cursor.getInt(1));
                temp.setEmitPointX(cursor.getInt(2));
                temp.setEmitPointY(cursor.getInt(3));
                temp.setImage(cursor.getString(4));
                temp.setImageWidth(cursor.getInt(5));
                temp.setImageHeight(cursor.getInt(6));
                temp.setAttackImage(cursor.getString(7));
                temp.setAttackImageWidth(cursor.getInt(8));
                temp.setAttackImageHeight(cursor.getInt(9));
                temp.setAttackSound(cursor.getString(10));
                temp.setDamage(cursor.getInt(11));
                result.add(temp);
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }

        return result;
    }

    public List<engines> getEngines(){
        List<engines> result = new ArrayList<>();
        Cursor cursor = db.rawQuery(SELECT_ALL_ENGINES, EMPTY_ARRAY_OF_STRINGS);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                engines temp = new engines();
                temp.setBaseSpeed(cursor.getInt(0));
                temp.setBaseTurnRate(cursor.getInt(1));
                temp.setAttachPointX(cursor.getInt(2));
                temp.setAttachPointY(cursor.getInt(3));
                temp.setImage(cursor.getString(4));
                temp.setImageWidth(cursor.getInt(5));
                temp.setImageHeight(cursor.getInt(6));
                result.add(temp);
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }

        return result;
    }

    public List<extraParts> getExtraParts(){
        List<extraParts> result = new ArrayList<>();
        Cursor cursor = db.rawQuery(SELECT_ALL_EXTRAPARTS, EMPTY_ARRAY_OF_STRINGS);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                extraParts temp = new extraParts();
                temp.setAttachPointX(cursor.getInt(0));
                temp.setAttachPointY(cursor.getInt(1));
                temp.setImage(cursor.getString(2));
                temp.setImageWidth(cursor.getInt(3));
                temp.setImageHeight(cursor.getInt(4));
                result.add(temp);
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }

        return result;
    }

    public List<levelAsteroids> getLevelAsteroids(){
        List<levelAsteroids> result = new ArrayList<>();
        Cursor cursor = db.rawQuery(SELECT_ALL_LEVELASTEROIDS, EMPTY_ARRAY_OF_STRINGS);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                levelAsteroids temp = new levelAsteroids();
                temp.setLevelNum(cursor.getInt(0));
                temp.setNumOfAsteroids(cursor.getInt(1));
                temp.setAsteroidId(cursor.getInt(2));
                result.add(temp);
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }

        return result;
    }

    public List<levelBckGndObjects> getlevelBckGndObjects(){
        List<levelBckGndObjects> result = new ArrayList<>();
        Cursor cursor = db.rawQuery(SELECT_ALL_LEVELBACKGNDOBJECTS, EMPTY_ARRAY_OF_STRINGS);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                levelBckGndObjects temp = new levelBckGndObjects();
                temp.setPositionX(cursor.getInt(0));
                temp.setPositionY(cursor.getInt(1));
                temp.setLvlBckGndObjectId(cursor.getInt(2));
                temp.setScale(cursor.getInt(3));
                temp.setLvlId(cursor.getInt(4));
                result.add(temp);
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }

        return result;
    }

    public List<levels> getLevels(){
        List<levels> result = new ArrayList<>();
        Cursor cursor = db.rawQuery(SELECT_ALL_LEVELS, EMPTY_ARRAY_OF_STRINGS);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                levels temp = new levels();
                temp.setLevelId(cursor.getInt(0));
                temp.setNumber(cursor.getInt(1));
                temp.setTitle(cursor.getString(2));
                temp.setHint(cursor.getString(3));
                temp.setWidth(cursor.getInt(4));
                temp.setHeight(cursor.getInt(5));
                temp.setMusic(cursor.getString(6));
                result.add(temp);
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }

        return result;
    }

    public List<mainBodies> getMainBodies(){
        List<mainBodies> result = new ArrayList<>();
        Cursor cursor = db.rawQuery(SELECT_ALL_MAINBODIES, EMPTY_ARRAY_OF_STRINGS);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                mainBodies temp = new mainBodies();
                temp.setCannonAttachX(cursor.getInt(0));
                temp.setCannonAttachY(cursor.getInt(1));
                temp.setEngineAttachX(cursor.getInt(2));
                temp.setEngineAttachY(cursor.getInt(3));
                temp.setExtraAttachX(cursor.getInt(4));
                temp.setExtraAttachY(cursor.getInt(5));
                temp.setImage(cursor.getString(6));
                temp.setImageWidth(cursor.getInt(7));
                temp.setImageHeight(cursor.getInt(8));
                result.add(temp);
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }

        return result;
    }

    public List<powerCores> getPowerCores(){
        List<powerCores> result = new ArrayList<>();
        Cursor cursor = db.rawQuery(SELECT_ALL_POWERCORES, EMPTY_ARRAY_OF_STRINGS);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                powerCores temp = new powerCores();
                temp.setCannonBoost(cursor.getInt(0));
                temp.setEngineBoost(cursor.getInt(1));
                temp.setImage(cursor.getString(2));
                result.add(temp);
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }

        return result;
    }

    /**
     * @param database
     * This will initially set what database we are going to use.
     */
    public void setDB(SQLiteDatabase database) {
        db = database;
    }

}


