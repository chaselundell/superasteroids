package edu.byu.cs.superasteroids.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.byu.cs.superasteroids.database.SuperAsteroidsDAO;

/**
 * Created by chaselundell on 2/23/2016.
 * This class will store all the information for the game.
 */
public class MainGame {
    public static MainGame _instance;
    public static void initialize(Context context, boolean loadFromObj, SQLiteDatabase database)
    {
        _instance = new MainGame(context, loadFromObj, database);
    }
    public static SuperAsteroidsDAO superAsteroidsDAO;
    private static List<asteroidTypes> asteroidTypesModel;
    private static List<bckGndObjectTypes> bckGndObjectTypesModel;
    private static Set<bullets> bulletsModel;
    private static List<cannons> cannonsModel;
    private static List<engines> enginesModel;
    private static List<extraParts> extraPartsModel;
    private static List<levelAsteroids> levelAsteroidsModel;
    private static List<levelBckGndObjects> levelBckGndObjectsModel;
    private static List<levels> levelsModel;
    private static List<mainBodies> mainBodiesModel;
    private static List<powerCores> powerCoresModel;
    private static ship shipModel;
    private static int quickPlayChosenIndex;

    public MainGame(Context context, boolean loadFromObj, SQLiteDatabase database){
        superAsteroidsDAO = new SuperAsteroidsDAO(database);
        asteroidTypesModel = new ArrayList<>();
        bckGndObjectTypesModel = new ArrayList<>();
        bulletsModel = new HashSet<>();
        cannonsModel = new ArrayList<>();
        enginesModel = new ArrayList<>();
        extraPartsModel = new ArrayList<>();
        levelAsteroidsModel = new ArrayList<>();
        levelBckGndObjectsModel = new ArrayList<>();
        levelsModel = new ArrayList<>();
        mainBodiesModel = new ArrayList<>();
        powerCoresModel = new ArrayList<>();
        shipModel = new ship();
        quickPlayChosenIndex = -1;

        initializeModels();
    }

    public void initializeModels(){
        asteroidTypesModel = superAsteroidsDAO.getAsteroidTypes();
        bckGndObjectTypesModel = superAsteroidsDAO.getBckGndObjectTypes();
        cannonsModel = superAsteroidsDAO.getCannons();
        enginesModel = superAsteroidsDAO.getEngines();
        extraPartsModel = superAsteroidsDAO.getExtraParts();
        levelAsteroidsModel = superAsteroidsDAO.getLevelAsteroids();
        levelBckGndObjectsModel = superAsteroidsDAO.getlevelBckGndObjects();
        levelsModel = superAsteroidsDAO.getLevels();
        mainBodiesModel = superAsteroidsDAO.getMainBodies();
        powerCoresModel = superAsteroidsDAO.getPowerCores();
        System.out.println("finished initializing models");
    }

    public static int getQuickPlayChosenIndex() {
        return quickPlayChosenIndex;
    }

    public static void setQuickPlayChosenIndex(int quickPlayChosenIndex) {
        MainGame.quickPlayChosenIndex = quickPlayChosenIndex;
    }

    public SuperAsteroidsDAO getSuperAsteroidsDAO() {
        return superAsteroidsDAO;
    }

    public void setSuperAsteroidsDAO(SuperAsteroidsDAO superAsteroidsDAO) {
        this.superAsteroidsDAO = superAsteroidsDAO;
    }

    public static ship getShipModel() {
        return shipModel;
    }

    public static void setShipModel(ship shipModel) {
        MainGame.shipModel = shipModel;
    }

    public static void setShipMainBodyImage(Integer image){
        shipModel.setMainBodyImage(image);
    }

    public static void setShipExtraPartImage(Integer image){
        shipModel.setShipExtraPartImage(image);
    }

    public static void setShipEngineImage(Integer image){
        shipModel.setShipEngineImage(image);
    }

    public static void setShipCannonImage(Integer image){
        shipModel.setShipCannonImage(image);
    }

    public static List<asteroidTypes> getAsteroidTypesModel() {
        return asteroidTypesModel;
    }

    public static void setAsteroidTypesModel(List<asteroidTypes> asteroidTypesModel) {
        MainGame.asteroidTypesModel = asteroidTypesModel;
    }

    public static List<bckGndObjectTypes> getBckGndObjectTypesModel() {
        return bckGndObjectTypesModel;
    }

    public static void setBckGndObjectTypesModel(List<bckGndObjectTypes> bckGndObjectTypesModel) {
        MainGame.bckGndObjectTypesModel = bckGndObjectTypesModel;
    }

    public static Set<bullets> getBulletsModel() {
        return bulletsModel;
    }

    public static void setBulletsModel(Set<bullets> bulletsModel) {
        MainGame.bulletsModel = bulletsModel;
    }

    public static List<cannons> getCannonsModel() {
        return cannonsModel;
    }

    public static void setCannonsModel(List<cannons> cannonsModel) {
        MainGame.cannonsModel = cannonsModel;
    }

    public static List<engines> getEnginesModel() {
        return enginesModel;
    }

    public static void setEnginesModel(List<engines> enginesModel) {
        MainGame.enginesModel = enginesModel;
    }

    public static List<extraParts> getExtraPartsModel() {
        return extraPartsModel;
    }

    public static void setExtraPartsModel(List<extraParts> extraPartsModel) {
        MainGame.extraPartsModel = extraPartsModel;
    }

    public static List<levelAsteroids> getLevelAsteroidsModel() {
        return levelAsteroidsModel;
    }

    public static void setLevelAsteroidsModel(List<levelAsteroids> levelAsteroidsModel) {
        MainGame.levelAsteroidsModel = levelAsteroidsModel;
    }

    public static List<levelBckGndObjects> getLevelBckGndObjectsModel() {
        return levelBckGndObjectsModel;
    }

    public static void setLevelBckGndObjectsModel(List<levelBckGndObjects> levelBckGndObjectsModel) {
        MainGame.levelBckGndObjectsModel = levelBckGndObjectsModel;
    }

    public static List<levels> getLevelsModel() {
        return levelsModel;
    }

    public static void setLevelsModel(List<levels> levelsModel) {
        MainGame.levelsModel = levelsModel;
    }

    public static List<mainBodies> getMainBodiesModel() {
        return mainBodiesModel;
    }

    public static void setMainBodiesModel(List<mainBodies> mainBodiesModel) {
        MainGame.mainBodiesModel = mainBodiesModel;
    }

    public static List<powerCores> getPowerCoresModel() {
        return powerCoresModel;
    }

    public static void setPowerCoresModel(List<powerCores> powerCoresModel) {
        MainGame.powerCoresModel = powerCoresModel;
    }
}
