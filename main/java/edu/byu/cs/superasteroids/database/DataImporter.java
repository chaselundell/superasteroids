package edu.byu.cs.superasteroids.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * Created by chaselundell on 2/11/2016.
 */
public class DataImporter implements edu.byu.cs.superasteroids.importer.IGameDataImporter{
    private SQLiteDatabase database;
    private Context baseContext;
    private DBOpenHelper dbOpenHelper;
    Database dBase;
    public DataImporter(Context baseContext){
        this.baseContext = baseContext;
        dbOpenHelper = new DBOpenHelper(baseContext);
        database = dbOpenHelper.getWritableDatabase();
        dBase = new Database(database);
    }

    public boolean importData(InputStreamReader reader) {
        try {
            dbOpenHelper.dropAndCreateAllTables(database);
            JSONObject rootObject = new JSONObject(makeString(reader));
            JSONObject asteroidsGame = rootObject.getJSONObject("asteroidsGame");

            JSONArray objects = asteroidsGame.getJSONArray("objects");
            dBase.importObjects(objects);

            JSONArray asteroids = asteroidsGame.getJSONArray("asteroids");
            dBase.importAsteroidTypes(asteroids);

            JSONArray levels = asteroidsGame.getJSONArray("levels");
            dBase.importLevels(levels);

            JSONArray mainBodies = asteroidsGame.getJSONArray("mainBodies");
            dBase.importMainBodies(mainBodies);

            JSONArray cannons = asteroidsGame.getJSONArray("cannons");
            dBase.importCannons(cannons);

            JSONArray eParts = asteroidsGame.getJSONArray("extraParts");
            dBase.importExtraParts(eParts);

            JSONArray engines = asteroidsGame.getJSONArray("engines");
            dBase.importEngines(engines);

            JSONArray pCores = asteroidsGame.getJSONArray("powerCores");
            dBase.importPowerCores(pCores);

            dBase.initializeModels(baseContext, database);
            return true;
        }catch(org.json.JSONException je){
            return false;
        }catch (java.io.IOException ioe){
            return false;
        }

    }

    private String makeString(Reader reader) throws IOException {

        StringBuilder sb = new StringBuilder();
        char[] buf = new char[512];

        int n = 0;
        while ((n = reader.read(buf)) > 0) {
            sb.append(buf, 0, n);
        }

        return sb.toString();
    }

}

