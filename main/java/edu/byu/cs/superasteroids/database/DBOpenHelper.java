package edu.byu.cs.superasteroids.database;

import android.content.Context;
import android.database.sqlite.*;
import android.util.Log;

import java.io.File;

/**
 * Created by chaselundell on 2/11/2016.
 * This class opens and creates the database when it is first created
 */
public class DBOpenHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "SuperAsteroids.sqlite";
    private static final int DB_VERSION = 1;
    private boolean dBaseExists;
    Context ctx;
    public DBOpenHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        ctx = context;
    }

    public void onCreate(SQLiteDatabase db) {
        dBaseExists = checkDatabase();
        if(!dBaseExists)
            dropAndCreateAllTables(db);
    }
    public boolean getDBaseExists(){
        return checkDatabase();
    }
    //check if database is created or not and if not, create it
    public boolean checkDatabase(){
        File dbFile = ctx.getDatabasePath(DB_NAME);
        Log.d("opendb", "in here");
        return dbFile.exists();
    }
    public void dropAndCreateAllTables(SQLiteDatabase db){
        db.execSQL(DROP_BACKGNDOBJECTTYPES_TABLE_IF_EXISTS);
        db.execSQL(CREATE_BACKGNDOBJECTTYPES_TABLE);
        db.execSQL(DROP_ASTEROIDTYPES_TABLE_IF_EXISTS);
        db.execSQL(CREATE_ASTEROIDTYPES_TABLE);
        db.execSQL(DROP_LEVELS_TABLE_IF_EXISTS);
        db.execSQL(CREATE_LEVELS_TABLE);
        db.execSQL(DROP_LEVELASTEROIDS_TABLE_IF_EXISTS);
        db.execSQL(CREATE_LEVELASTEROIDS_TABLE);
        db.execSQL(DROP_LEVELBACKGNDOBJECTS_TABLE_IF_EXISTS);
        db.execSQL(CREATE_LEVELBACKGNDOBJECTS_TABLE);
        db.execSQL(DROP_MAINBODIES_TABLE_IF_EXISTS);
        db.execSQL(CREATE_MAINBODIES_TABLE);
        db.execSQL(DROP_POWERCORES_TABLE_IF_EXISTS);
        db.execSQL(CREATE_POWERCORES_TABLE);
        db.execSQL(DROP_CANNONS_TABLE_IF_EXISTS);
        db.execSQL(CREATE_CANNONS_TABLE);
        db.execSQL(DROP_ENGINES_TABLE_IF_EXISTS);
        db.execSQL(CREATE_ENGINES_TABLE);
        db.execSQL(DROP_EXTRAPARTS_TABLE_IF_EXISTS);
        db.execSQL(CREATE_EXTRAPARTS_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}
    public static final String DROP_BACKGNDOBJECTTYPES_TABLE_IF_EXISTS =
            "DROP TABLE IF EXISTS BACKGNDOBJECTTYPES";
    public static final String CREATE_BACKGNDOBJECTTYPES_TABLE =
            "CREATE TABLE BACKGNDOBJECTTYPES " +
                    "(" +
                    "   bckGndObjectId integer not null," +
                    "   imgPath varchar(255) not null" +
                    ")";
    public static final String DROP_ASTEROIDTYPES_TABLE_IF_EXISTS =
            "DROP TABLE IF EXISTS ASTEROIDTYPES";
    public static final String CREATE_ASTEROIDTYPES_TABLE =
            "CREATE TABLE ASTEROIDTYPES " +
                    "(" +
                    "    id integer not null," +
                    "    name varchar(255)," +
                    "    image varchar(255)," +
                    "    imageWidth integer not null," +
                    "    imageHeight integer not null," +
                    "    type varchar(255)" +
                    ")";
    public static final String DROP_LEVELS_TABLE_IF_EXISTS =
            "DROP TABLE IF EXISTS LEVELS";
    public static final String CREATE_LEVELS_TABLE =
            "CREATE TABLE LEVELS " +
                    "(" +
                    "    levelId integer not null," +
                    "    number integer not null," +
                    "    title varchar(255)," +
                    "    hint varchar(255)," +
                    "    width integer not null," +
                    "    height integer not null," +
                    "    music varchar(255)" +
                    ")";
    public static final String DROP_LEVELASTEROIDS_TABLE_IF_EXISTS =
            "DROP TABLE IF EXISTS LEVELASTEROIDS";
    public static final String CREATE_LEVELASTEROIDS_TABLE =
            "CREATE TABLE LEVELASTEROIDS " +
                    "(" +
                    "    levelNum integer not null," +
                    "    numOfAsteroids integer not null," +
                    "    asteroidId integer not null" +
                    ")";
    public static final String DROP_LEVELBACKGNDOBJECTS_TABLE_IF_EXISTS =
            "DROP TABLE IF EXISTS LEVELBACKGNDOBJECTS";
    public static final String CREATE_LEVELBACKGNDOBJECTS_TABLE =
            "CREATE TABLE LEVELBACKGNDOBJECTS " +
                    "(" +
                    "    positionX integer not null," +
                    "   positionY integer not null," +
                    "   lvlBckGndObjectId integer not null," +
                    "   scale integer not null," +
                    "   lvlId integer not null" +
                    ")";
    public static final String DROP_MAINBODIES_TABLE_IF_EXISTS =
            "DROP TABLE IF EXISTS MAINBODIES";
    public static final String CREATE_MAINBODIES_TABLE =
            "CREATE TABLE MAINBODIES " +
                    "(" +
                    "    cannonAttachX integer not null," +
                    "    cannonAttachY integer not null," +
                    "    engineAttachX integer not null," +
                    "    engineAttachY integer not null," +
                    "    extraAttachX integer not null," +
                    "    extraAttachY integer not null," +
                    "    image varchar(255)," +
                    "    imageWidth integer not null," +
                    "    imageHeight integer not null" +
                    ")";
    public static final String DROP_POWERCORES_TABLE_IF_EXISTS =
            "DROP TABLE IF EXISTS POWERCORES";
    public static final String CREATE_POWERCORES_TABLE =
            "CREATE TABLE POWERCORES " +
                    "(" +
                    "    cannonBoost integer not null," +
                    "    engineBoost integer not null," +
                    "    image varchar(255)" +
                    ")";
    public static final String DROP_CANNONS_TABLE_IF_EXISTS =
            "DROP TABLE IF EXISTS CANNONS";
    public static final String CREATE_CANNONS_TABLE =
            "CREATE TABLE CANNONS " +
                    "(" +
                    "    attachPointX integer not null," +
                    "    attachPointY integer not null," +
                    "    emitPointX integer not null," +
                    "    emitPointY integer not null," +
                    "    image varchar(255)," +
                    "    imageWidth integer not null," +
                    "    imageHeight integer not null," +
                    "    attackImage varchar(255)," +
                    "    attackImageWidth integer not null," +
                    "    attackImageHeight integer not null," +
                    "    attackSound varchar(255)," +
                    "    damage integer not null" +
                    ")";
    public static final String DROP_ENGINES_TABLE_IF_EXISTS =
            "DROP TABLE IF EXISTS ENGINES";
    public static final String CREATE_ENGINES_TABLE =
            "CREATE TABLE ENGINES " +
                    "(" +
                    "    baseSpeed integer not null," +
                    "    baseTurnRate integer not null," +
                    "    attachPointX integer not null," +
                    "    attachPointY integer not null," +
                    "    image varchar(255)," +
                    "    imageWidth integer not null," +
                    "    imageHeight integer not null" +
                    ")";
    public static final String DROP_EXTRAPARTS_TABLE_IF_EXISTS =
            "DROP TABLE IF EXISTS EXTRAPARTS";
    public static final String CREATE_EXTRAPARTS_TABLE =
            "CREATE TABLE EXTRAPARTS " +
                    "(" +
                    "    attachPointX integer not null," +
                    "    attachPointY integer not null," +
                    "    image varchar(255)," +
                    "    imageWidth integer not null," +
                    "    imageHeight integer not null" +
                    ")";
}
