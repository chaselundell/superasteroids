package edu.byu.cs.superasteroids.model;

/**
 * Created by chaselundell on 2/11/2016.
 */
public class mainBodies {
    private int cannonAttachX;
    private int cannonAttachY;
    private int engineAttachX;
    private int engineAttachY;
    private int extraAttachX;
    private int extraAttachY;
    private String image;
    private int imageWidth;
    private int imageHeight;
    private Integer convertedImage;

    public mainBodies(){
        cannonAttachX = 0;
        cannonAttachY = 0;
        engineAttachX = 0;
        engineAttachY = 0;
        extraAttachX = 0;
        extraAttachY = 0;
        image = "";
        imageWidth = 0;
        imageHeight = 0;
        convertedImage = 0;

    }

    public int getConvertedImage() {
        return convertedImage;
    }

    public void setConvertedImage(Integer convertedImage) {
        this.convertedImage = convertedImage;
    }

    public int getCannonAttachX() {
        return cannonAttachX;
    }

    public void setCannonAttachX(int cannonAttachX) {
        this.cannonAttachX = cannonAttachX;
    }

    public int getCannonAttachY() {
        return cannonAttachY;
    }

    public void setCannonAttachY(int cannonAttachY) {
        this.cannonAttachY = cannonAttachY;
    }

    public int getEngineAttachX() {
        return engineAttachX;
    }

    public void setEngineAttachX(int engineAttachX) {
        this.engineAttachX = engineAttachX;
    }

    public int getEngineAttachY() {
        return engineAttachY;
    }

    public void setEngineAttachY(int engineAttachY) {
        this.engineAttachY = engineAttachY;
    }

    public int getExtraAttachX() {
        return extraAttachX;
    }

    public void setExtraAttachX(int extraAttachX) {
        this.extraAttachX = extraAttachX;
    }

    public int getExtraAttachY() {
        return extraAttachY;
    }

    public void setExtraAttachY(int extraAttachY) {
        this.extraAttachY = extraAttachY;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getImageWidth() {
        return imageWidth;
    }

    public void setImageWidth(int imageWidth) {
        this.imageWidth = imageWidth;
    }

    public int getImageHeight() {
        return imageHeight;
    }

    public void setImageHeight(int imageHeight) {
        this.imageHeight = imageHeight;
    }
}
