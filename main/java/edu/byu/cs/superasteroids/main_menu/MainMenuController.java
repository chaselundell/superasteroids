package edu.byu.cs.superasteroids.main_menu;

import java.util.ArrayList;
import java.util.List;

import edu.byu.cs.superasteroids.base.IView;
import edu.byu.cs.superasteroids.content.ContentManager;
import edu.byu.cs.superasteroids.model.MainGame;
import edu.byu.cs.superasteroids.model.cannons;
import edu.byu.cs.superasteroids.model.engines;
import edu.byu.cs.superasteroids.model.extraParts;
import edu.byu.cs.superasteroids.model.mainBodies;
import edu.byu.cs.superasteroids.model.powerCores;
import edu.byu.cs.superasteroids.model.ship;

/**
 * Created by chaselundell on 3/1/2016.
 */
public class MainMenuController implements IMainMenuController {
    IMainMenuView view;
    boolean dbExists;
    private List<cannons> cannonParts = new ArrayList<>();
    private List<engines> engineParts = new ArrayList<>();
    private List<extraParts> extraPartsParts = new ArrayList<>();
    private List<powerCores> powerCoreParts = new ArrayList<>();
    private List<mainBodies> mainBodyParts = new ArrayList<>();
    public MainMenuController(IMainMenuView view, boolean databaseExists){
        this.view = view;
        dbExists = databaseExists;
    }

    @Override
    public void onQuickPlayPressed() {
        if(dbExists) {
            makeShip();
            view.startGame();
        }
    }

    private void makeShip(){
        ship gameShip = new ship();
        mainBodyParts = MainGame.getMainBodiesModel();
        powerCoreParts = MainGame.getPowerCoresModel();
        extraPartsParts = MainGame.getExtraPartsModel();
        engineParts = MainGame.getEnginesModel();
        cannonParts = MainGame.getCannonsModel();
        int shipToGet = 1;
        gameShip.setMainBody(mainBodyParts.get(shipToGet));
        gameShip.setExtraPart(extraPartsParts.get(shipToGet));
        gameShip.setCannon(cannonParts.get(shipToGet));
        gameShip.setEngine(engineParts.get(shipToGet));
        gameShip.setPowerCore(powerCoreParts.get(shipToGet));
        MainGame.setQuickPlayChosenIndex(shipToGet);
        MainGame.setShipModel(gameShip);
    }
    @Override
    public IView getView() {
        return null;
    }

    @Override
    public void setView(IView view) {

    }
}
