package edu.byu.cs.superasteroids.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import edu.byu.cs.superasteroids.model.*;

/**
 * Created by chaselundell on 2/11/2016.
 * This class will interact with the DAOs to import information
 * and then get the information into the models later.
 */
public class Database {
    public SuperAsteroidsDAO superAsteroidsDAO;
    private boolean isCreated;
    //Constructor
    public Database(SQLiteDatabase database) {
        superAsteroidsDAO = new SuperAsteroidsDAO(database);
        isCreated = false;
    }

    public void initializeModels(Context context, SQLiteDatabase dBase){
        MainGame.initialize(context, true, dBase);
    }

    public void importObjects(JSONArray objects) throws JSONException {
        for (int i = 0; i < objects.length(); ++i) {
            bckGndObjectTypes type = new bckGndObjectTypes();
            String imagePath = objects.getString(i);
            type.setImgPath(imagePath);
            type.setBckGndObjectId(i);
            SuperAsteroidsDAO.storeBckGndObjectTypes(type);
        }
    }

    public void importAsteroidTypes(JSONArray asteroids) throws JSONException {
        for (int i = 0; i < asteroids.length(); ++i) {
            asteroidTypes type = new asteroidTypes();
            JSONObject asteroidTypesJson = asteroids.getJSONObject(i);
            type.setId(i);
            type.setName(asteroidTypesJson.getString("name"));
            type.setImage(asteroidTypesJson.getString("image"));
            type.setImageWidth(asteroidTypesJson.getInt("imageWidth"));
            type.setImageHeight(asteroidTypesJson.getInt("imageHeight"));
            type.setType(asteroidTypesJson.getString("type"));
            SuperAsteroidsDAO.storeAsteroidTypes(type);
        }
    }

    public void importLevelObjects(JSONArray levelObjects, int lvlId) throws JSONException {
        for (int i = 0; i < levelObjects.length(); ++i) {
            levelBckGndObjects lvlObject = new levelBckGndObjects();

            JSONObject tempLvlObject = levelObjects.getJSONObject(i);
            String positions = (String) tempLvlObject.get("position");
            String[] posArray = positions.split(",");
            lvlObject.setPositionX(Integer.parseInt(posArray[0]));
            lvlObject.setPositionY(Integer.parseInt(posArray[1]));
            lvlObject.setLvlBckGndObjectId(tempLvlObject.getInt("objectId"));
            lvlObject.setScale(tempLvlObject.getInt("scale"));
            lvlObject.setLvlId(lvlId);
            SuperAsteroidsDAO.storeLevelBckGndObjects(lvlObject);
        }
    }

    public void importLevelAsteroids(JSONArray lvlAsteroids, int lvlId) throws JSONException {
        for (int i = 0; i < lvlAsteroids.length(); ++i) {
            levelAsteroids lvlAsteroid = new levelAsteroids();
            JSONObject tempLvlAsteroid = lvlAsteroids.getJSONObject(i);
            lvlAsteroid.setNumOfAsteroids(tempLvlAsteroid.getInt("number"));
            lvlAsteroid.setAsteroidId(tempLvlAsteroid.getInt("asteroidId"));
            lvlAsteroid.setLevelNum(lvlId);
            SuperAsteroidsDAO.storeLevelAsteroids(lvlAsteroid);
        }
    }

    public void importLevels(JSONArray lvls) throws JSONException {
        for (int i = 0; i < lvls.length(); ++i) {
            levels lvl = new levels();
            JSONObject lvlJson = lvls.getJSONObject(i);
            lvl.setLevelId(i);
            lvl.setTitle(lvlJson.getString("title"));
            lvl.setHint(lvlJson.getString("hint"));
            lvl.setWidth(lvlJson.getInt("width"));
            lvl.setHeight(lvlJson.getInt("height"));
            lvl.setMusic(lvlJson.getString("music"));
            SuperAsteroidsDAO.storeLevels(lvl);

            JSONArray levelAsteroids = lvlJson.getJSONArray("levelAsteroids");
            importLevelAsteroids(levelAsteroids, i);

            JSONArray levelObjects = lvlJson.getJSONArray("levelObjects");
            importLevelObjects(levelObjects, i);
        }
    }

    public void importMainBodies(JSONArray mainBodies) throws JSONException {
        for (int i = 0; i < mainBodies.length(); ++i) {
            JSONObject mainB = mainBodies.getJSONObject(i);
            edu.byu.cs.superasteroids.model.mainBodies mBody = new mainBodies();
            String positions = (String) mainB.get("cannonAttach");
            String[] posArray = positions.split(",");
            mBody.setCannonAttachX(Integer.parseInt(posArray[0]));
            mBody.setCannonAttachY(Integer.parseInt(posArray[1]));
            positions = (String) mainB.get("engineAttach");
            posArray = positions.split(",");
            mBody.setEngineAttachX(Integer.parseInt(posArray[0]));
            mBody.setEngineAttachY(Integer.parseInt(posArray[1]));
            positions = (String) mainB.get("extraAttach");
            posArray = positions.split(",");
            mBody.setExtraAttachX(Integer.parseInt(posArray[0]));
            mBody.setExtraAttachY(Integer.parseInt(posArray[1]));
            mBody.setImage(mainB.getString("image"));
            mBody.setImageWidth(mainB.getInt("imageWidth"));
            mBody.setImageHeight(mainB.getInt("imageHeight"));
            SuperAsteroidsDAO.storeMainBodies(mBody);
        }
    }

    public void importCannons(JSONArray cannons) throws JSONException {
        for (int i = 0; i < cannons.length(); ++i) {
            JSONObject cannonsJson = cannons.getJSONObject(i);
            edu.byu.cs.superasteroids.model.cannons cannonsToStore = new cannons();
            String positions = (String) cannonsJson.get("attachPoint");
            String[] posArray = positions.split(",");
            cannonsToStore.setAttachPointX(Integer.parseInt(posArray[0]));
            cannonsToStore.setAttachPointY(Integer.parseInt(posArray[1]));
            positions = (String) cannonsJson.get("emitPoint");
            posArray = positions.split(",");
            cannonsToStore.setEmitPointX(Integer.parseInt(posArray[0]));
            cannonsToStore.setEmitPointY(Integer.parseInt(posArray[1]));
            cannonsToStore.setImage(cannonsJson.getString("image"));
            cannonsToStore.setImageWidth(cannonsJson.getInt("imageWidth"));
            cannonsToStore.setImageHeight(cannonsJson.getInt("imageHeight"));
            cannonsToStore.setAttackImage(cannonsJson.getString("attackImage"));
            cannonsToStore.setAttackImageWidth(cannonsJson.getInt("attackImageWidth"));
            cannonsToStore.setAttackImageHeight(cannonsJson.getInt("attackImageHeight"));
            cannonsToStore.setAttackSound(cannonsJson.getString("attackSound"));
            cannonsToStore.setDamage(cannonsJson.getInt("damage"));
            SuperAsteroidsDAO.storeCannons(cannonsToStore);
        }
    }

    public void importExtraParts(JSONArray eParts) throws JSONException {
        for (int i = 0; i < eParts.length(); ++i) {
            JSONObject extraPartsJson = eParts.getJSONObject(i);
            extraParts ePartsToStore = new extraParts();
            String positions = (String) extraPartsJson.get("attachPoint");
            String[] posArray = positions.split(",");
            ePartsToStore.setAttachPointX(Integer.parseInt(posArray[0]));
            ePartsToStore.setAttachPointY(Integer.parseInt(posArray[1]));
            ePartsToStore.setImage(extraPartsJson.getString("image"));
            ePartsToStore.setImageWidth(extraPartsJson.getInt("imageWidth"));
            ePartsToStore.setImageHeight(extraPartsJson.getInt("imageHeight"));
            SuperAsteroidsDAO.storeExtraParts(ePartsToStore);
        }
    }

    public void importEngines(JSONArray engines) throws JSONException {
        for (int i = 0; i < engines.length(); ++i) {
            JSONObject engineJson = engines.getJSONObject(i);
            edu.byu.cs.superasteroids.model.engines engineToStore = new engines();
            engineToStore.setBaseSpeed(engineJson.getInt("baseSpeed"));
            engineToStore.setBaseTurnRate(engineJson.getInt("baseTurnRate"));
            String positions = (String) engineJson.get("attachPoint");
            String[] posArray = positions.split(",");
            engineToStore.setAttachPointX(Integer.parseInt(posArray[0]));
            engineToStore.setAttachPointY(Integer.parseInt(posArray[1]));
            engineToStore.setImage(engineJson.getString("image"));
            engineToStore.setImageWidth(engineJson.getInt("imageWidth"));
            engineToStore.setImageHeight(engineJson.getInt("imageHeight"));
            SuperAsteroidsDAO.storeEngines(engineToStore);
        }
    }

    public void importPowerCores(JSONArray pCores) throws JSONException {
        for (int i = 0; i < pCores.length(); ++i) {
            JSONObject pCoreJson = pCores.getJSONObject(i);
            powerCores pCoreToStore = new powerCores();
            pCoreToStore.setCannonBoost(pCoreJson.getInt("cannonBoost"));
            pCoreToStore.setEngineBoost(pCoreJson.getInt("engineBoost"));
            pCoreToStore.setImage(pCoreJson.getString("image"));
            SuperAsteroidsDAO.storePowerCores(pCoreToStore);
        }
    }

}
