package edu.byu.cs.superasteroids.model;

/**
 * Created by chaselundell on 2/11/2016.
 */
public class powerCores {
    private int cannonBoost;
    private int engineBoost;
    private String image;
    private Integer convertedImage;

    public powerCores(){
        cannonBoost = 0;
        engineBoost = 0;
        image = "";
    }

    public int getCannonBoost() {
        return cannonBoost;
    }

    public Integer getConvertedImage() {
        return convertedImage;
    }

    public void setConvertedImage(Integer convertedImage) {
        this.convertedImage = convertedImage;
    }

    public void setCannonBoost(int cannonBoost) {
        this.cannonBoost = cannonBoost;
    }

    public int getEngineBoost() {
        return engineBoost;
    }

    public void setEngineBoost(int engineBoost) {
        this.engineBoost = engineBoost;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
