package edu.byu.cs.superasteroids.model;

/**
 * Created by chaselundell on 2/11/2016.
 */
public class levelBckGndObjects extends visibleObjects{
    private int positionX;
    private int positionY;
    private int lvlBckGndObjectId;
    private int scale;
    private int lvlId;

    public levelBckGndObjects(){
        positionX = 0;
        positionY = 0;
        lvlBckGndObjectId = 0;
        scale = 0;
        lvlId = 0;

    }

    public int getPositionX() {
        return positionX;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }

    public int getLvlBckGndObjectId() {
        return lvlBckGndObjectId;
    }

    public void setLvlBckGndObjectId(int lvlBckGndObjectId) {
        this.lvlBckGndObjectId = lvlBckGndObjectId;
    }

    public int getScale() {
        return scale;
    }

    public void setScale(int scale) {
        this.scale = scale;
    }

    public int getLvlId() {
        return lvlId;
    }

    public void setLvlId(int lvlId) {
        this.lvlId = lvlId;
    }
}
